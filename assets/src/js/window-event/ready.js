// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      // Scroll to the top
      var scroll_to_top = $('.lg-scroll-to-top');

      if(scroll_to_top[0]){
        scroll_to_top.on('click', function(e){
          e.preventDefault();
          var scrollTo = $('html');

          KCollection.headerScrollTo(scrollTo, scroll_settings);
        });
      }
      // End scroll to the top

      //Mobile Toggle
      $('.mobile-toggle').on('click', function(){
      	$('.navbar-collapse').fadeToggle();
      });

      $('.menu-item.dropdown').on('mouseover', function(e){
        $(this).find('.dropdown-menu').addClass('show');
      });

      $('.menu-item.dropdown').on('mouseleave', function(e){
        $(this).find('.dropdown-menu').removeClass('show');
      });

      $('.menu-item.dropdown').on('click', function(e){
        $(this).find('.dropdown-menu').toggleClass('show');
      });

      //Mega Menu
      if($('.mega-menu-toggle')[0]){
        $('.mobile-toggle').on('click', function(){
          $('.mega-menu-toggle').click();
        });
      }

      $('[data-toggle="offcanvas"]').on('click', function () {
        $(this).toggleClass('is-active');
        $('.offcanvas-collapse').toggleClass('open');
      })

    });
}(jQuery));
