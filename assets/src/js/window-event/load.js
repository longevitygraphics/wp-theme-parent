// Windows Load Handler

(function($) {

    $(window).on('load', function(){

    	if($('html').hasClass('sticky-header') && !$('html').hasClass('sticky-header-overlap')){
    		sticky_header_initialize( function(){
                scroll_settings = {
                  duration: 600,
                  offset: $('.sticky-header')[0] ? (-1) * $('#site-header').outerHeight() : 0
                };

                lg_loading(false);

                // Hash scroll when document load
                var hash = window.location.hash;
                if($(hash)[0]){
                  KCollection.headerScrollTo($(hash), scroll_settings);
                }
                // End hash scroll when document load
            } );
    	}else{
    		lg_loading(false);

            scroll_settings = {
              duration: 600,
              offset: $('.sticky-header')[0] ? (-1) * $('#site-header').outerHeight() : 0
            };

            // Hash scroll when document load
            var hash = window.location.hash;
            if($(hash)[0]){
              KCollection.headerScrollTo($(hash), scroll_settings);
            }
            // End hash scroll when document load
    	}

        
      // Hash scroll

      $(document).on('click', 'a', function(e){
        var target = $(this).attr('href');

        // url example '#test'
        if(typeof target !== typeof undefined && target !== false && target.startsWith('#') && $(this).attr('role') != 'tab'){
          e.preventDefault();
          if($(target)[0]){
            KCollection.headerScrollTo($(target), scroll_settings);
          }
        // url example 'http://test.test#test', where current domain is test.test
        }else if(typeof target !== typeof undefined && target !== false && target.startsWith('http')){
          var current_domain = window.location.hostname.replace('www.', '');
          if(target.indexOf(current_domain) != -1){
            var href = target.split('#')[1];
            
            if($('#' + href)[0]){
              e.preventDefault();
              KCollection.headerScrollTo($('#' + href), scroll_settings);
            }
          }
        }
      });

      // End hash scroll

    	$('.viewport-animation').each(function(){
    		$(this).attr('top', $(this).offset().top);
    	});

    	view_port_animation();

    });

}(jQuery));