// Window Resize Handler

(function($) {

    $(window).on('resize', function(){

    	if($('html').hasClass('sticky-header') && !$('html').hasClass('sticky-header-overlap')){
    		sticky_header_initialize();
    	}

    })

}(jQuery));