
$ = jQuery;
var scroll_settings = null;

function lg_loading(status){
	var loading_dom = $('.lg-loading-screen');
	if(loading_dom[0]){
		if(status){
			if(!loading_dom.is(':visible')){
				loading_dom.fadeIn();
				$('body').addClass('lock');
			}
		}else{
			if(loading_dom.is(':visible')){
				loading_dom.fadeOut();
				$('body').removeClass('lock');
			}
		}
	}
}

function sticky_header_initialize( callback ){
	var site_header = $('#site-header');
	var site_content = $('#site-content');
	var headerHeight = site_header.outerHeight();

	site_content.css('paddingTop', headerHeight);

	if(callback != null){
		callback();
	}

}

function view_port_animation(){
	$('.viewport-animation').each(function(){
		var element = $(this);
		var animation = element.attr('animation');
			if(KCollection.screenPosition(element) == 0 && animation && !element.hasClass('viewport-animated')){
		  element.addClass(animation).addClass('viewport-animated');
		}else if(KCollection.screenPosition(element) == 1 && animation && element.hasClass('viewport-animated')){
		  element.removeClass('viewport-animated').removeClass(animation);
		}
	});
}
