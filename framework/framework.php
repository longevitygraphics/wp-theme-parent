<?php

	class LongevityFramework{

		protected $views = array();
		var $path = '';

		function __construct(){

			$this->init();
			$this->path = get_template_directory() . '/framework';

			/*if(constant('lg_framework')){*/
				add_action('admin_bar_menu', array( $this, 'admin_bar_menu' ), 999);
				add_action( 'admin_menu', array( $this, 'admin_menu' ) );
			/*}*/
			
			add_action( 'wp_enqueue_scripts', array($this, 'front_enqueue_scripts') );
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ));
			add_action( 'admin_init', array($this, 'lg_client_role'));
			add_action("after_switch_theme", array($this, "flushing_rewrite_rules"));
			add_filter('acf/settings/remove_wp_meta_box', '__return_true');

		}

		function init(){
			require_once "include/helper.php";
			require_once "include/model.php";
			require_once "include/ajax.php";
			require_once "include/restful.php";
			require_once "include/functions.php";
			require_once "include/theme-settings.php";
			require_once "include/fontawesome/fontawesome.php";
			require_once "include/hooks.php";
			require_once "include/tinymce/tinymce.php";
			require_once "include/speed-optimization.php";

		}

		function flushing_rewrite_rules(){
			flush_rewrite_rules();
		}

		function load_view() {
	        // current_filter() also returns the current action
	        $current_views = $this->views[current_filter()];
	        include($this->path . '/views/' . $current_views . '.php');
	    }

		function admin_menu(){
			require_once "include/admin-menu.php";
		}

		function admin_bar_menu($wp_admin_bar){
			require_once "include/admin-bar-menu.php";
		}

		function front_enqueue_scripts(){
	    	wp_register_script( 'longevity_front_script', get_template_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), filemtime(get_template_directory() . "/assets/dist/js/script.min.js"), true );
	    	wp_enqueue_script( 'longevity_front_script' );
		}

		function admin_enqueue_scripts(){
			wp_register_style( 'longevity_admin_style', get_template_directory_uri() . '/assets/dist/css/admin.min.css', false, filemtime(get_template_directory() . '/assets/dist/css/admin.min.css') );
	    	wp_enqueue_style( 'longevity_admin_style' );

	    	wp_register_script( 'longevity_framework_script', get_template_directory_uri() . "/assets/dist/js/framework.min.js", array('jquery'), filemtime(get_template_directory() . "/assets/dist/js/framework.min.js") );
	    	wp_enqueue_script( 'longevity_framework_script' );

	    	wp_register_script( 'longevity_admin_script', get_template_directory_uri() . "/assets/dist/js/admin.min.js", array('jquery'), filemtime(get_template_directory() . "/assets/dist/js/admin.min.js") );
	    	wp_enqueue_script( 'longevity_admin_script' );

			wp_localize_script(
				'longevity_ajax_url',
				'ajax_url',
				array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
			);

			if (is_admin ()){
				wp_enqueue_media();
			}
		}

		function lg_client_role(){

		  // Add LG Client Role

		  $result = add_role(
		      'lg_client',
		      __( 'LG Client', 'longevity-template' ),
		      array()
		  );

		  $role = get_role( 'lg_client' );
		  $role->add_cap( 'activate_plugins', false );
		  $role->add_cap( 'delete_others_pages', true );
		  $role->add_cap( 'delete_others_posts', true );
		  $role->add_cap( 'delete_pages', true );
		  $role->add_cap( 'delete_posts', true );
		  $role->add_cap( 'delete_private_pages', true );
		  $role->add_cap( 'delete_private_posts', true );
		  $role->add_cap( 'delete_published_pages', true );
		  $role->add_cap( 'delete_published_posts', true );
		  $role->add_cap( 'edit_dashboard', false );
		  $role->add_cap( 'edit_others_pages', true );
		  $role->add_cap( 'edit_others_posts', true );
		  $role->add_cap( 'edit_pages', true );
		  $role->add_cap( 'edit_posts', true );
		  $role->add_cap( 'edit_private_pages', true );
		  $role->add_cap( 'edit_private_posts', true );
		  $role->add_cap( 'edit_published_pages', true );
		  $role->add_cap( 'edit_published_posts', true );
		  $role->add_cap( 'edit_theme_options', true );
		  $role->add_cap( 'export', false );
		  $role->add_cap( 'import', false );
		  $role->add_cap( 'list_users', false );
		  $role->add_cap( 'manage_categories', true );
		  $role->add_cap( 'manage_links', true );
		  $role->add_cap( 'manage_options', false );
		  $role->add_cap( 'moderate_comments', false );
		  $role->add_cap( 'promote_users', false );
		  $role->add_cap( 'publish_pages', true );
		  $role->add_cap( 'publish_posts', true );
		  $role->add_cap( 'read_private_pages', true );
		  $role->add_cap( 'read_private_posts', true );
		  $role->add_cap( 'read', true );
		  $role->add_cap( 'remove_users', false );
		  $role->add_cap( 'switch_themes', false );
		  $role->add_cap( 'upload_files', true );
		  $role->add_cap( 'customize', true );
		  $role->add_cap( 'delete_site', false );
		  $role->add_cap( 'update_core', false );
		  $role->add_cap( 'update_plugins', false );
		  $role->add_cap( 'update_themes', false );
		  $role->add_cap( 'install_plugins', false );
		  $role->add_cap( 'install_themes', false );
		  $role->add_cap( 'upload_plugins', false );
		  $role->add_cap( 'upload_themes', false );
		  $role->add_cap( 'delete_themes', false );
		  $role->add_cap( 'delete_plugins', false );
		  $role->add_cap( 'edit_plugins', false );
		  $role->add_cap( 'edit_themes', false );
		  $role->add_cap( 'edit_files', false );
		  $role->add_cap( 'edit_users', false );
		  $role->add_cap( 'create_users', false );
		  $role->add_cap( 'delete_users', false );
		  $role->add_cap( 'unfiltered_html', false );

		}
	}

	new LongevityFramework();

?>