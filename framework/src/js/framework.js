require('angular');
require('angular-material');
require('angular-animate');
require('angular-aria');
require('angular-messages');
require('angular-sanitize');

//ngApp config
var framework = angular.module('framework', ['ngMaterial', 'ngMessages', 'ngSanitize'])
	/*.config(function($interpolateProvider){
	    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})*/
	.config(function($locationProvider){
		$locationProvider.html5Mode({
			  enabled: true,
			  requireBase: false
			}).hashPrefix('*');
	})
	.config(function($mdThemingProvider) {
		$mdThemingProvider
			.definePalette('white', {
			    '50': 'ffffff',
			    '100': 'ffffff',
			    '200': 'ffffff',
			    '300': 'ffffff',
			    '400': 'ffffff',
			    '500': 'ffffff',
			    '600': 'ffffff',
			    '700': 'ffffff',
			    '800': 'ffffff',
			    '900': 'ffffff',
			    'A100': 'ffffff',
			    'A200': 'ffffff',
			    'A400': 'ffffff',
			    'A700': 'ffffff',
			    'contrastDefaultColor': 'dark'
			  });

	  	$mdThemingProvider.theme('default')
		    .backgroundPalette('white');
	});


//Controllers
var pluginCtrl = require('./controller/pluginCtrl');
var templateCtrl = require('./controller/templateCtrl');

framework.controller('pluginCtrl', ['$scope', '$http', '$q', pluginCtrl]);
framework.controller('templateCtrl', ['$scope', '$http', '$q', templateCtrl]);
