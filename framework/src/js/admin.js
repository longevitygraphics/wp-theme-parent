// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){

    	// Start grid system script

    	$('body').on('change', '.acf-field[data-name^="item_per_row"] select', function(){
    		var value = $(this).find('option:selected').val();
    		var size = $(this).closest('.acf-field[data-name^=item_per_row]').attr('data-name');

    		switch (size) {
			  case 'item_per_row_large':
			    size = "lg";
			    break;
			  case 'item_per_row_medium':
			    size = "md";
			    break;
			  case 'item_per_row_small':
			    size = "sm";
			    break;
			  case 'item_per_row_extra_small':
			    size = "xs";
			    break;
			}

    		var element = $(this).closest('.acf-field[data-name=grid_responsive]').siblings('.acf-field[data-name=grid_list]').find('.acf-repeater .acf-table tbody');

    		if(!element.hasClass('row')){
    			element.addClass('row');
    		}

    		element.find('.acf-row:not(.acf-clone)').each(function(){
    			if($(this).is('[class*="col-'+ size +'-"]')){
    				var classes = $(this).attr('class').split(' ');
    				for (var i = 0; i < classes.length; i++) {
    					if(classes[i].startsWith('col-' + size + '-')){
    						$(this).removeClass(classes[i]);
    					}
    				}
    			}

    			$(this).addClass('col-' + size + '-' + value);
    		});
    		
    	});

    	// dom mutation observer
    	var observer = new MutationObserver(function(mutations) {
			mutations.forEach(function(mutation) {
				var node = mutation.addedNodes[0];
				if(mutation.type == 'childList' && node){
					var parent = $(node).closest('.acf-field[data-name=grid_list]')[0];
					if($(node).hasClass('acf-row') && parent){
						$(parent).siblings('.acf-field[data-name=grid_responsive]')
							.find('select')
							.trigger('change');
					}
					if($(node).hasClass('acf-tab-wrap') && parent){
						$(node).siblings('.acf-field[data-name=container]')
							.find('input[type=radio][value=container-wide]')
							.prop('checked', true)
							.closest('label')
							.addClass('selected')
							.closest('li')
							.siblings()
							.find('label')
							.removeClass('selected')
							.find('input[type=radio]')
							.prop('checked', false);
					}
				}
			});    
		});
		 
		var observerConfig = {
			characterData: true,
			characterDataOldValue: true,
			childList: true,
			subtree: true
		};
		 
		observer.observe(document.body, observerConfig);

    	$('.acf-field[data-name^="item_per_row"] select').trigger('change');

    	// End grid system script
    });

}(jQuery));