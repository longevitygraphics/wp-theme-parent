module.exports = function($scope, $http, $q) {

	var plugin_list_url = "https://form-test.longevitystaging.com/wp-framework/plugins/?format=json";
	var plugin_categories = "https://form-test.longevitystaging.com/wp-framework/plugins_category/?format=json";
	var site_plugins_url = "/?rest_route=/longevity_framework/v1/plugins";

	$scope.loading = false;

	load();

	function load(){

		$scope.loading = true;
		$scope.plugin_categories = [];
		$scope.plugin_list = [];
		$scope.site_plugins = [];
		var data;

		$q.all([$http.get(plugin_categories),$http.get(plugin_list_url),$http.get(site_plugins_url)])
		      .then(function(response){

		      	$scope.plugin_categories = response[0].data;
		      	$scope.plugin_list = response[1].data;

		        data = response[2].data;
		        for(var key in data){
		        	var temp = data[key];
		        	temp.pluginFile = key;
		        	$scope.site_plugins.push(temp);
		        }

		        $scope.plugin_list = $scope.plugin_list.map(function(item){
		        	var found = false;
		        	for (var i = 0; i < $scope.site_plugins.length; i++) {
		        		if(item.folderName + '/' + item.pluginFile == $scope.site_plugins[i].PluginFile){
		        			item.installed = true;
		        			item.active = $scope.site_plugins[i].Active;
		        		}
		        	}
		        	return item;
		        });

		        $scope.loading = false;
		 })
	}

	function plugin_install(data){

		$scope.loading = true;

		$http({
	        method: 'POST',
	        url: ajaxurl,
	        params: {
	       		action : 'lg_plugin_install',
	       		data : JSON.stringify(data)
	        }
	    }).then(function (response){
 			load();
	    },function (error){
 
	    });
	}

	function plugin_uninstall(data){

		$scope.loading = true;

		$http({
	        method: 'POST',
	        url: ajaxurl,
	        params: {
	       		action : 'lg_plugin_uninstall',
	       		data : JSON.stringify(data)
	        }
	    }).then(function (response){
 			load();
	    },function (error){
 
	    });
	}

	function plugin_activate(data){

		$scope.loading = true;

		$http({
	        method: 'POST',
	        url: ajaxurl,
	        params: {
	       		action : 'lg_plugin_activate',
	       		data : data.folderName + '/' + data.pluginFile
	        }
	    }).then(function (response){
 			load();
	    },function (error){
 
	    });
	}

	function plugin_deactivate(data){

		$scope.loading = true;

		$http({
	        method: 'POST',
	        url: ajaxurl,
	        params: {
	       		action : 'lg_plugin_deactivate',
	       		data : data.folderName + '/' + data.pluginFile
	        }
	    }).then(function (response){
 			load();
	    },function (error){
 
	    });
	}

	$scope.plugin_install = plugin_install;
	$scope.plugin_uninstall = plugin_uninstall;
	$scope.plugin_activate = plugin_activate;
	$scope.plugin_deactivate = plugin_deactivate;

};