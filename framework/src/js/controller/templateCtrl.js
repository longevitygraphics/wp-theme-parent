module.exports = function($scope, $http, $q) {

	var template_list_url = "https://form-test.longevitystaging.com/wp-framework/themes/?format=json";
	var template_category_url = "https://form-test.longevitystaging.com/wp-framework/themes_category/?format=json";

	$scope.loading = false;

	load();

	function load(){

		$scope.loading = true;
		$scope.template_list = [];
		$scope.template_category_list = [];

		$q.all([$http.get(template_category_url),$http.get(template_list_url)])
	    .then(function(response) {
	       	
	       	$scope.template_category_list = response[0].data;
	       	$scope.template_list = response[1].data;
	        $scope.loading = false;
	    });
	}

	function template_install(data){

		$scope.loading = true;

		$http({
	        method: 'POST',
	        url: ajaxurl,
	        params: {
	       		action : 'lg_template_install',
	       		data : JSON.stringify(data)
	        }
	    }).then(function (response){
 			load();
	    },function (error){
 
	    });
	}

	$scope.template_install = template_install;

};