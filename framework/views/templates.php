<?php

	@include "layout/before.php";

?>

<div class="container-fluid" ng-app="framework" ng-controller="templateCtrl">

	<ul class="nav nav-tabs" id="myTab" role="tablist">
	  <li ng-repeat="category in template_category_list" class="nav-item">
	    <a class="nav-link" ng-class="{'active' : $index == 0}" id="{{category.slug}}-tab" data-toggle="tab" href="#{{category.slug}}" role="tab" aria-controls="{{category.slug}}" aria-selected="true">{{category.name}}</a>
	  </li>
	</ul>

	<div class="tab-content" id="myTabContent">

	  <div class="tab-pane fade show" ng-repeat="category in template_category_list" ng-class="{'active' : $index == 0}" id="{{category.slug}}" role="tabpanel" aria-labelledby="{{category.slug}}-tab">
		<div class="py-4">
			<div class="row lg-list-items">
				<div ng-repeat="data in template_list" ng-if="data.category==category.slug" class="col-xl-2 col-lg-3 col-md-4 col-sm-6 mb-4">
					<img class="d-block" style="width: 100%;" ng-src="{{data.image}}">
					<div class="overlay d-flex align-items-center justify-content-center flex-column">
						<a class="btn-green btn btn-equal" ng-click="template_install(data);">Install</a>
					</div>
					<div class="py-2 text-center bg-blue text-white">{{data.name}}</div>
				</div>
			</div>
		</div>
	  </div>

	</div>

	<div class="loading" ng-show="loading">
		<?php lg_loading(); ?>
	</div>
</div>

<?php

	@include "layout/after.php";

?>