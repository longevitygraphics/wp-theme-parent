<?php

	@include "layout/before.php";
	global $wpdb;

?>

	<div class="container-fluid">

		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="server-info-tab" data-toggle="tab" href="#server-info" role="tab" aria-controls="server-info" aria-selected="true">Server Info</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="wordpress-tab" data-toggle="tab" href="#wordpress" role="tab" aria-controls="wordpress" aria-selected="false">Wordpress Info</a>
		  </li>
		   <li class="nav-item">
		    <a class="nav-link" id="database-tab" data-toggle="tab" href="#database" role="tab" aria-controls="database" aria-selected="false">Database Info</a>
		  </li>
		</ul>

		<div class="tab-content" id="myTabContent">

		  <div class="tab-pane fade show active" id="server-info" role="tabpanel" aria-labelledby="server-info-tab">
			<div class="py-4">

				<table class="table">
					<tr>
						<td>Operation System</td>
						<td><?php echo php_uname(); ?></td>
					</tr>
					<tr>
						<td>PHP Version</td>
						<td><?php echo phpversion(); ?></td>
					</tr>
				</table>

			</div>
		  </div>

		  <div class="tab-pane fade" id="wordpress" role="tabpanel" aria-labelledby="wordpress-tab">
			<div class="py-4">
				
				<table class="table">
					<tr>
						<td>Wordpress Version</td>
						<td><?php echo get_bloginfo('version'); ?></td>
					</tr>
					<tr>
						<td>Debug</td>
						<td><?php echo WP_DEBUG ? 'true' : 'false'; ?></td>
					</tr>
					<tr>
						<td>Debug Log</td>
						<td><?php echo WP_DEBUG_LOG ? 'true' : 'false'; ?></td>
					</tr>
				</table>

			</div>
		  </div>

		  <div class="tab-pane fade" id="database" role="tabpanel" aria-labelledby="database-tab">
			<div class="py-4">
				
				<table class="table">
					<tr>
						<td>Host</td>
						<td><?php echo DB_HOST ?></td>
					</tr>
					<tr>
						<td>Database Name</td>
						<td><?php echo DB_NAME ?></td>
					</tr>
					<tr>
						<td>Database User</td>
						<td><?php echo DB_USER ?></td>
					</tr>
					<tr>
						<td>Database Charset</td>
						<td><?php echo DB_CHARSET ?></td>
					</tr>
					<tr>
						<td>Database Collate</td>
						<td><?php echo DB_COLLATE ?></td>
					</tr>
					<tr>
						<td>Database Prefix</td>
						<td><?php echo $wpdb->prefix ?></td>
					</tr>
				</table>

			</div>
		  </div>

		</div>

		<div class="loading" ng-show="loading">
			<?php lg_loading(); ?>
		</div>
	</div>

	<script>
	(function($) {
			
		$(window).on('load', function(){
			$('.loading').fadeOut();
		});

	}(jQuery));
	</script>

<?php

	@include "layout/after.php";

?>