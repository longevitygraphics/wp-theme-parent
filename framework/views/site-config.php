<?php

	@include "layout/before.php";

?>

	<div class="container-fluid py-4 lg-site-config">
		<div class="row">
			<div class="mb-3 mb-md-0 col-lg-3 col-md-4">
				<!-- Bootstrap Tabs -->
				<ul class="nav flex-column nav-pills m-0" id="myTab" role="tablist">


				  <li class="nav-item">
				    <a class="nav-link active" id="contact-info-tab" data-toggle="tab" href="#contact-info" role="tab" aria-controls="contact-info" aria-selected="true">Contact Info</a>
				  </li>

				  <li class="nav-item">
				    <a class="nav-link" id="social-media-tab" data-toggle="tab" href="#social-media" role="tab" aria-controls="social-media" aria-selected="true">Social Media</a>
				  </li>

				  <li class="nav-item">
				    <a class="nav-link" id="analytics-tab" data-toggle="tab" href="#analytics" role="tab" aria-controls="analytics" aria-selected="true">Analytics Tracking</a>
				  </li>

				  <li class="nav-item">
				    <a class="nav-link" id="instructions-tab" data-toggle="tab" href="#instructions" role="tab" aria-controls="instructions" aria-selected="true">Instructions</a>
				  </li>

				</ul>
			</div>
			<div class="col-lg-9 col-md-8">
				<!-- Bootstrap Tab Content -->
				<div class="tab-content" id="myTabContent">

					<div class="tab-pane fade show active" id="contact-info" role="tabpanel" aria-labelledby="contact-info-tab">

						<h3>Address</h3>
						<hr>
						<table class="table">
							<tr>
								<td width="250">Address</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_address" name="lg_option_address">
								</td>
							</tr>
							<tr>
								<td width="250">City</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_city" name="lg_option_city">
								</td>
							</tr>
							<tr>
								<td width="250">Province</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_province" name="lg_option_province">
								</td>
							</tr>
							<tr>
								<td width="250">Country</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_country" name="lg_option_country">
								</td>
							</tr>
							<tr>
								<td width="250">Postal Code</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_postcode" name="lg_option_postcode">
								</td>
							</tr>
						</table>

						<br><br>
						<h3>Contact</h3>
						<hr>

						<table class="table">
							<tr>
								<td width="250">Contact Person</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_contact_person" name="lg_option_contact_person">
								</td>
							</tr>
							<tr>
								<td width="250">Job Title</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_job_title" name="lg_option_job_title">
								</td>
							</tr>
							<tr>
								<td width="250">Phone Main</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_phone_main" name="lg_option_phone_main">
								</td>
							</tr>
							<tr>
								<td width="250">Phone Alt</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_phone_alt" name="lg_option_phone_alt">
								</td>
							</tr>
							<tr>
								<td width="250">Fax</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_fax" name="lg_option_fax">
								</td>
							</tr>
							<tr>
								<td width="250">Email</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_email" name="lg_option_email">
								</td>
							</tr>
						</table>

					</div>


					<div class="tab-pane fade" id="social-media" role="tabpanel" aria-labelledby="social-media-tab">

						<h3>Social Media</h3>
						<hr>
						<table class="table">
							<tr>
								<td width="250">Facebook</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_social_media_facebook" name="lg_option_social_media_facebook">
									<input class="fontawesome-icon-input d-none lg-options" type="text" id="lg_option_social_media_facebook_icon" name="lg_option_social_media_facebook_icon">
								</td>
								<td><div class="icon" name="lg_option_social_media_facebook"></div></td>
								<td><a class="btn fontawesome-set-icon">Set Icon</a></td>
							</tr>
							<tr>
								<td width="250">Twitter</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_social_media_twitter" name="lg_option_social_media_twitter">
									<input class="fontawesome-icon-input d-none lg-options" type="text" id="lg_option_social_media_twitter_icon" name="lg_option_social_media_twitter_icon">
								</td>
								<td><div class="icon" name="lg_option_social_media_twitter"></div></td>
								<td><a class="btn fontawesome-set-icon">Set Icon</a></td>
							</tr>
							<tr>
								<td width="250">Instagram</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_social_media_instagram" name="lg_option_social_media_instagram">
									<input class="fontawesome-icon-input d-none lg-options" type="text" id="lg_option_social_media_instagram_icon" name="lg_option_social_media_instagram_icon">
								</td>
								<td><div class="icon" name="lg_option_social_media_instagram"></div></td>
								<td><a class="btn fontawesome-set-icon">Set Icon</a></td>
							</tr>
							<tr>
								<td width="250">Linkedin</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_social_media_linkedin" name="lg_option_social_media_linkedin">
									<input class="fontawesome-icon-input d-none lg-options" type="text" id="lg_option_social_media_linkedin_icon" name="lg_option_social_media_linkedin_icon">
								</td>
								<td><div class="icon" name="lg_option_social_media_linkedin"></div></td>
								<td><a class="btn fontawesome-set-icon">Set Icon</a></td>
							</tr>
							<tr>
								<td width="250">Google+</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_social_media_google_plus" name="lg_option_social_media_google_plus">
									<input class="fontawesome-icon-input d-none lg-options" type="text" id="lg_option_social_media_google_plus_icon" name="lg_option_social_media_google_plus_icon">
								</td>
								<td><div class="icon" name="lg_option_social_media_google_plus"></div></td>
								<td><a class="btn fontawesome-set-icon">Set Icon</a></td>
							</tr>
							<tr>
								<td width="250">Youtube</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_social_media_youtube" name="lg_option_social_media_youtube">
									<input class="fontawesome-icon-input d-none lg-options" type="text" id="lg_option_social_media_youtube_icon" name="lg_option_social_media_youtube_icon">
								</td>
								<td><div class="icon" name="lg_option_social_media_youtube"></div></td>
								<td><a class="btn fontawesome-set-icon">Set Icon</a></td>
							</tr>
							<tr>
								<td width="250">Pinterest</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_social_media_pinterest" name="lg_option_social_media_pinterest">
									<input class="fontawesome-icon-input d-none lg-options" type="text" id="lg_option_social_media_pinterest_icon" name="lg_option_social_media_pinterest_icon">
								</td>
								<td><div class="icon" name="lg_option_social_media_pinterest"></div></td>
								<td><a class="btn fontawesome-set-icon">Set Icon</a></td>
							</tr>
						</table>

					</div>

					<div class="tab-pane fade" id="analytics" role="tabpanel" aria-labelledby="analytics-tab">

						<table class="table">
							<tr>
								<td width="250">Google Tag Manager (Recommended)</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_google_tag_manager_id" name="lg_option_google_tag_manager_id">
								</td>
							</tr>
							<tr>
								<td width="250">Facebook Pixel</td>
								<td>
									<input type="text" class="lg-options" id="lg_option_facebook_pixel_id" name="lg_option_facebook_pixel_id">
								</td>
							</tr>
						</table>

					</div>

					<div class="tab-pane fade" id="instructions" role="tabpanel" aria-labelledby="instructions-tab">

						<?php require_once get_template_directory() . '/framework/include/theme-settings/lg-theme-documentation.php'; ?>

					</div>

				</div>
		
				<a class="btn btn-primary mt-5" id="submit">Update</a>

			</div>
		</div>

	</div>

	<script>
		(function($) {
			
		    $(document).ready(function(){

		    	lg_get_config();

		    	$('#submit').on('click', function(){
		    		var elements = $('.lg-options').toArray();
			    	elements = elements.map(function(item){
			    		if($(item).hasClass('wp-editor-area')){
			    			return {"name": $(item).prop('name'), "value": encodeURI(tinymce.get($(item).prop('id')).getContent())};
			    		}else{
			    			return {"name": $(item).prop('name'), "value": $(item).val()};
			    		}
			    		
			    	});

			    	var data = { 
						'action': 'lg_update_config',
						'data': JSON.stringify(elements)
					};

					jQuery.post(ajaxurl, data, function(response) {
						//location.reload();
						alert('successfully updated!');
					});
		    	});

	    	    if ($('.set_image_button').length > 0) {
			        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			            $(document).on('click', '.set_image_button', function(e) {
			                e.preventDefault();
			                var button = $(this);
			                var input = button.siblings('input');
			                var image = button.siblings('img');
			                wp.media.editor.send.attachment = function(props, attachment) {
			                    input.val(attachment.url);
			                    image.prop('src', attachment.url);
			                };
			                wp.media.editor.open(button);
			                return false;
			            });
			        }
			    }

				$('.fa-close').on('click', function(){
					$(this).closest('#fontawesome-modal-wrapper').fadeOut();
				});

				$('ul[role=tablist] .nav-item .nav-link').on('click', function(e){
					e.preventDefault();
					$(this).addClass('active').closest('.nav-item').siblings().find('.nav-link').removeClass('active');
					location.hash = $(this).attr('href');
				});

				$(window).on('hashchange', function () {      
					$('ul[role=tablist] .nav-item .nav-link[href="' + location.hash + '"]').addClass('active').closest('.nav-item').siblings().find('.nav-link').removeClass('active');  
					$('.tab-content ' + location.hash).addClass('show').addClass('active').siblings().removeClass('show').removeClass('active');
				}).trigger('hashchange');
		      
		    });

		    function lg_get_config(){	

		    	var elements = $('.lg-options').toArray();
			    	elements = elements.map(function(item){
			    		var type = (($(item).prop("type") == "select-one") ? "dropdown" : $(item).prop("type"));
			    		return {"type": type, "name": $(item).prop('name')};
			    	});

		    	var data = {
					'action': 'lg_get_config',
					'data': JSON.stringify(elements)
				};

				jQuery.post(ajaxurl, data, function(response) {
					data = JSON.parse(response);
					
					for(var i = 0; i < data.length; i++){
						var element = $('#'+data[i].name);

						switch (data[i].type) {
						    case "dropdown":
						        element.find('option[value=' + data[i].value + ']').prop('selected', 'selected');
						        break;
						    case "textarea":
						    	if(data[i].value){
						    		tinyMCE.get(data[i].name).setContent(decodeURI(data[i].value));
						    	}
						        break;
						    case "text":
						    	if(element.hasClass('image')){
						    		element.siblings('img').prop('src', data[i].value);
						    	}
						    	if(element.hasClass('fontawesome-icon-input')){
						    		element.parent().next().find('.icon').append('<i class="' + data[i].value +'"></i>');
						    	}
						    	
						    	if(data[i].value){
						    		element.val(data[i].value);
						    	}
						    	
						    	break;
						}
					}

					$('.loading').fadeOut();
				});
				
		    }



		}(jQuery));

	</script>

	<?php include get_template_directory() . '/framework/include/fontawesome/view.php' ?>

<?php

	@include "layout/after.php";

?>