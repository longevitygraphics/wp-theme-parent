<?php

	@include "layout/before.php";

?>

<div class="container-fluid" ng-app="framework" ng-controller="pluginCtrl">

	<ul class="nav nav-tabs" id="myTab" role="tablist">
	  <li class="nav-item">
	    <a class="nav-link active" id="install-plugins-tab" data-toggle="tab" href="#install-plugins" role="tab" aria-controls="install-plugins" aria-selected="true">Install Plugins</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="current-plugins-tab" data-toggle="tab" href="#current-plugins" role="tab" aria-controls="current-plugins" aria-selected="false">Current Plugins</a>
	  </li>
	</ul>

	<div class="tab-content" id="myTabContent">

	  <div class="tab-pane fade show active" id="install-plugins" role="tabpanel" aria-labelledby="install-plugins-tab">
		<div class="py-4">
			<section ng-repeat="category in plugin_categories">
				<h1 class="mb-3 text-uppercase h3 font-weight-bold">{{category.name}}</h1>
				<div class="row {{category.slug}}">
					<div ng-repeat="data in plugin_list" ng-if="data.category==category.slug" class="col-xl-2 col-lg-3 col-md-4 col-sm-6 mb-4 lg-list-items">
						<img class="d-block" style="width: 100%;" ng-src="{{data.image}}">
						<div class="overlay d-flex align-items-center justify-content-center flex-column">
							<a class="btn-green btn btn-equal" ng-click="plugin_install(data)" ng-if="!data.installed">Install</a>
							<a class="btn-green btn btn-equal" ng-click="plugin_install(data)" href="" ng-if="data.installed">Re-Install</a>
							<a class="btn-green btn btn-equal" ng-click="plugin_activate(data)" ng-if="data.installed && !data.active">Activate</a>
							<a class="btn-danger btn btn-equal" ng-click="plugin_deactivate(data)" ng-if="data.installed && data.active">Deactivate</a>
							<a class="btn-danger btn btn-equal" ng-click="plugin_uninstall(data)" ng-if="data.installed && !data.active" href="">Delete</a>
						</div>
						<div class="py-2 text-center bg-blue text-white">{{data.name}}</div>
					</div>
				</div>
			</section>

		</div>
	  </div>

	  <div class="tab-pane fade" id="current-plugins" role="tabpanel" aria-labelledby="current-plugins-tab-tab">
	  		<div id="site-plugins" class="py-4">
				<h1 class="mb-3 text-uppercase h3 font-weight-bold">Installed Plugins</h1>
				<table class="table table-striped table-bordered">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Version</th>
						<th>Author</th>
						<th>Status</th>
					</tr>
					<tr ng-repeat="data in site_plugins | orderBy : '-Active'">
						<td width="300">{{data.Name}}</td>
						<td ng-bind-html="data.Description"></td>
						<td>{{data.Version}}</td>
						<td>{{data.Author}}</td>
						<td ng-if="data.Active" class="text-success">Active</td>
						<td ng-if="!data.Active" class="text-warning">Inactive</td>
					</tr>
				</table>
			</div>
	  </div>
	</div>

	<div class="loading" ng-show="loading">
		<?php lg_loading(); ?>
	</div>
</div>

<?php

	@include "layout/after.php";

?>