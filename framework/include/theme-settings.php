<?php
	use  Longevity\Framework\Helper;
	class LGThemeSettings{

		function __construct(){
			add_action( 'wp_head', array($this, 'tag_manager_head') );
			add_action( 'wp_footer', array($this, 'tag_manager_footer') );
			add_action( 'wp_head', array($this, 'google_analytics') );
			add_action( 'wp_head', array($this, 'facebook_pixel') );

			add_shortcode( 'lg-logo', array($this, 'sc_logo') );
			add_shortcode( 'lg-logo-alt', array($this, 'sc_logo_alt') );

			add_shortcode( 'lg-address', array($this, 'sc_address') );
			add_shortcode( 'lg-city', array($this, 'sc_city') );
			add_shortcode( 'lg-province', array($this, 'sc_province') );
			add_shortcode( 'lg-country', array($this, 'sc_country') );
			add_shortcode( 'lg-postcode', array($this, 'sc_postcode') );

			add_shortcode( 'lg-contact-person', array($this, 'sc_contact_person') );
			add_shortcode( 'lg-job-title', array($this, 'sc_job_title') );
			add_shortcode( 'lg-phone-main', array($this, 'sc_phone_main') );
			add_shortcode( 'lg-phone-alt', array($this, 'sc_phone_alt') );
			add_shortcode( 'lg-fax', array($this, 'sc_fax') );
			add_shortcode( 'lg-email', array($this, 'sc_email') );
			add_shortcode( 'lg-social-media', array($this, 'sc_social_media') );
		}

		function tag_manager_head() {
			$google_tag_manager = get_option('lg_option_google_tag_manager_id');
			if($google_tag_manager){
			ob_start();
		 ?>

			<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','<?php echo $google_tag_manager; ?>');</script>
			<!-- End Google Tag Manager -->

		<?php
			echo ob_get_clean();
			}
		}

		function tag_manager_footer() {
			$google_tag_manager = get_option('lg_option_google_tag_manager_id');

			if($google_tag_manager){
			ob_start();
		 ?>

			<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $google_tag_manager; ?>"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->

		<?php
			echo ob_get_clean();
			}
		}

		function google_analytics() {
			$google_analytics = get_option('lg_option_google_analytics_id');

			if($google_analytics){
			ob_start();
		 ?>
			<!-- Global Site Tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $google_analytics; ?>"></script>
			<script>
			  window.dataLayer = window.dataLayer || [];
			  function gtag(){dataLayer.push(arguments);}
			  gtag('js', new Date());

			  gtag('config', '<?php echo $google_analytics; ?>');
			</script>
		<?php
			echo ob_get_clean();
			}
		}

		function facebook_pixel() {
			$facebook_pixel = get_option('lg_option_facebook_pixel_id');

			if($facebook_pixel){
			ob_start();
		 ?>
			<!-- Facebook Pixel Code -->
			<script>
			  !function(f,b,e,v,n,t,s)
			  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			  n.queue=[];t=b.createElement(e);t.async=!0;
			  t.src=v;s=b.getElementsByTagName(e)[0];
			  s.parentNode.insertBefore(t,s)}(window, document,'script',
			  'https://connect.facebook.net/en_US/fbevents.js');
			  fbq('init', '<?php echo $facebook_pixel; ?>');
			  fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
			  src="https://www.facebook.com/tr?id=<?php echo $facebook_pixel; ?>&ev=PageView&noscript=1"
			/></noscript>
			<!-- End Facebook Pixel Code -->
		<?php
			echo ob_get_clean();
			}
		}

		function sc_address(){
			return get_option('lg_option_address') ? get_option('lg_option_address') : '';
		}

		function sc_city(){
			return get_option('lg_option_city') ? get_option('lg_option_city') : '';
		}

		function sc_province(){
			return get_option('lg_option_province') ? get_option('lg_option_province') : '';
		}

		function sc_country(){
			return get_option('lg_option_country') ? get_option('lg_option_country') : '';
		}

		function sc_postcode(){
			return get_option('lg_option_postcode') ? get_option('lg_option_postcode') : '';
		}

		function sc_contact_person(){
			return get_option('lg_option_contact_person') ? get_option('lg_option_contact_person') : '';
		}

		function sc_job_title(){
			return get_option('lg_option_job_title') ? get_option('lg_option_job_title') : '';
		}

		function sc_phone_main(){
			return get_option('lg_option_phone_main') ? get_option('lg_option_phone_main') : '';
		}

		function sc_phone_alt(){
			return get_option('lg_option_phone_alt') ? get_option('lg_option_phone_alt') : '';
		}

		function sc_fax(){
			return get_option('lg_option_fax') ? get_option('lg_option_fax') : '';
		}

		function sc_email(){
			return get_option('lg_option_email') ? get_option('lg_option_email') : '';
		}

		function sc_logo(){
			return get_option('lg_option_logo') ? get_option('lg_option_logo') : '';
		}

		function sc_logo_alt(){
			return get_option('lg_option_logo_alt') ? get_option('lg_option_logo_alt') : '';
		}

		function sc_social_media() {

		    $lg_option_social_media_facebook = get_option('lg_option_social_media_facebook');
		    $lg_option_social_media_twitter = get_option('lg_option_social_media_twitter');
		    $lg_option_social_media_instagram = get_option('lg_option_social_media_instagram');
		    $lg_option_social_media_linkedin = get_option('lg_option_social_media_linkedin');
		    $lg_option_social_media_google_plus = get_option('lg_option_social_media_google_plus');
		    $lg_option_social_media_youtube = get_option('lg_option_social_media_youtube');
		    $lg_option_social_media_pinterest = get_option('lg_option_social_media_pinterest');

		    $lg_option_social_media_facebook_icon = get_option('lg_option_social_media_facebook_icon');
		    $lg_option_social_media_twitter_icon = get_option('lg_option_social_media_twitter_icon');
		    $lg_option_social_media_instagram_icon = get_option('lg_option_social_media_instagram_icon');
		    $lg_option_social_media_linkedin_icon = get_option('lg_option_social_media_linkedin_icon');
		    $lg_option_social_media_google_plus_icon = get_option('lg_option_social_media_google_plus_icon');
		    $lg_option_social_media_youtube_icon = get_option('lg_option_social_media_youtube_icon');
		    $lg_option_social_media_pinterest_icon = get_option('lg_option_social_media_pinterest_icon');

	    	ob_start();?><div class="lg-social-media-cont">
	            <ul class="lg-social-media">
	            	<?php if($lg_option_social_media_facebook): ?>
		                <li><a href="<?php echo $lg_option_social_media_facebook; ?>" target="_blank"><i class="<?php echo $lg_option_social_media_facebook_icon; ?>"></i></a></li>
		            <?php endif; ?>
		            <?php if($lg_option_social_media_twitter): ?>
		                <li><a href="<?php echo $lg_option_social_media_twitter; ?>" target="_blank"><i class="<?php echo $lg_option_social_media_twitter_icon; ?>"></i></a></li>
		            <?php endif; ?>
		            <?php if($lg_option_social_media_instagram): ?>
		                <li><a href="<?php echo $lg_option_social_media_instagram; ?>" target="_blank"><i class="<?php echo $lg_option_social_media_instagram_icon; ?>"></i></a></li>
		            <?php endif; ?>
		            <?php if($lg_option_social_media_linkedin): ?>
		                <li><a href="<?php echo $lg_option_social_media_linkedin; ?>" target="_blank"><i class="<?php echo $lg_option_social_media_linkedin_icon; ?>"></i></a></li>
		            <?php endif; ?>
		            <?php if($lg_option_social_media_google_plus): ?>
		                <li><a href="<?php echo $lg_option_social_media_google_plus; ?>" target="_blank"><i class="<?php echo $lg_option_social_media_google_plus_icon; ?>"></i></a></li>
		            <?php endif; ?>
		            <?php if($lg_option_social_media_youtube): ?>
		                <li><a href="<?php echo $lg_option_social_media_youtube; ?>" target="_blank"><i class="<?php echo $lg_option_social_media_youtube_icon; ?>"></i></a></li>
		            <?php endif; ?>
		            <?php if($lg_option_social_media_pinterest): ?>
		                <li><a href="<?php echo $lg_option_social_media_pinterest; ?>" target="_blank"><i class="<?php echo $lg_option_social_media_pinterest_icon; ?>"></i></a></li>
		            <?php endif; ?>
	            </ul>
	        </div><?php
	        return ob_get_clean();
	    }
	}

	new LGThemeSettings();

?>
