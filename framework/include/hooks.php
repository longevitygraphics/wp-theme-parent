<?php
use Longevity\Framework\Helper;

	class LGHooks{

		function __construct(){
			add_action('activate_advanced-custom-fields-pro/acf.php', array($this, 'lg_acf_activate'));
			// add_action('activate_gravityforms/gravityforms.php', array($this, 'lg_gf_activate'));
			add_filter('body_class', array($this, 'body_class'), 10);
			add_action('html_class', array($this, 'html_class'), 10);
			add_action('wp_body_start', array($this, 'lg_loading_screen_html'), 10);
			add_action('wp_utility_bar', array($this, 'lg_utility_bar'), 10);
			add_action('wp_utility_left', array($this, 'lg_utility_left'), 10);
			add_action('wp_utility_right', array($this, 'lg_utility_right'), 10);
			add_action('wp_utility_center', array($this, 'lg_utility_center'), 10);
			add_action('wp_header', array($this, 'lg_meta'), 10);
			add_action('wp_footer', array($this, 'lg_scroll_to_top'), 10);

			if(lg_dependency_check()){
				add_filter('acf/settings/load_json', array($this, 'acf_json_load_point'));
				add_filter('acf/load_field/key=field_5bd37498afe6c', array($this, 'acf_fc_load_fields'));
				add_filter('acf/load_field/key=field_5c61cad38dabe', array($this, 'acf_fc_load_template_component'));
				add_filter('acf/fields/flexible_content/layout_title', array($this, 'lg_flexible_content_layout_title'), 10, 4);
			}
		}

		function lg_acf_activate(){
			acf_pro_update_license('b3JkZXJfaWQ9MTEwNjQ0fHR5cGU9ZGV2ZWxvcGVyfGRhdGU9MjAxNy0wNy0xOSAyMDo1NzozNw==');
		}

		function acf_json_load_point( $paths ) {
		    $paths[] = get_template_directory() . '/acf-json';
		    return $paths;
		}

		function acf_fc_load_fields($field){
			if(function_exists('get_current_screen') && get_current_screen()->post_type != 'acf-field-group' || !function_exists('get_current_screen')){

				//Add parent layout to child and child grid
				$fields = get_field_object('field_5b3f987f738ec')['layouts'];
				$grid_layout = array();

				if($field && is_array($field)){
					foreach ($field['layouts'] as $key => $layout) {
						array_push($grid_layout, $layout);
					}
				}
				
				if($fields && is_array($fields)){
					foreach ($fields as $key => $layout) {		
						if($layout['key'] != '5b4f7f78fd824'){
							array_push($grid_layout, $layout);
						}
						array_push($field['layouts'], $layout);
					}
				}

				if($field['layouts'] && is_array($field['layouts'])){
					foreach ($field['layouts'] as $key => $layouts) {
						if($layouts['key'] == '5b4f7f78fd824'){
							$field['layouts'][$key]['sub_fields'][1]['sub_fields'][1]['layouts'] = $grid_layout;
						}
					}
				}
			}
			return $field;
		}

		function acf_fc_load_template_component($field){
			if(!file_exists(get_stylesheet_directory() . '/layouts/include-template-data.php')){
				return $field;
			}

			require_once get_stylesheet_directory() . '/layouts/include-template-data.php';
			global $template_to_load;

			if($template_to_load && is_array($template_to_load)){
				$field['choices'] = $template_to_load;
			}

			return $field;
		}

		function acf_fc_load_fields_grid($field){
			if(function_exists('get_current_screen') && get_current_screen()->post_type != 'acf-field-group' || !function_exists('get_current_screen')){
				/*$fields = get_field_object('field_5b3f987f738ec')['layouts'];
				if($fields && is_array($fields)){
					foreach ($fields as $key => $layout) {
						array_push($field['layouts'], $layout);
					}
				}*/
			}

			return $field;
		}

		function body_class($classes) {
			$lg_site_config_loading_screen = get_option('lg_option_loading_screen');

			if($lg_site_config_loading_screen == 'enable'){
				array_push($classes, 'lock');
			}
	        return $classes;
		}

		function html_class() {
			$classes = '';

			$lg_site_config_loading_screen = get_option('lg_option_loading_screen');
			$lg_option_sticky_header = get_option('lg_option_sticky_header');

			if($lg_site_config_loading_screen == 'enable'){
				$classes .= 'loading-screen ';
			}

			if($lg_option_sticky_header == 'all' || ($lg_option_sticky_header == 'home' && is_front_page())){
				$classes .= 'sticky-header ';
			}

	        echo 'class="' . $classes . '"';
		}

		function lg_loading_screen_html(){
		  	$lg_site_config_loading_screen = get_option('lg_option_loading_screen');
		  	if($lg_site_config_loading_screen == 'enable'){
		    	echo lg_loading();
		  	}
		}

		function lg_scroll_to_top(){
		  	$lg_option_scroll_to_top = get_option('lg_option_scroll_to_top');
		  	if($lg_option_scroll_to_top == 'all' || $lg_option_scroll_to_top == 'mobile'){
		    	ob_start();
		    	?>
		      	<div class="lg-scroll-to-top <?php if($lg_option_scroll_to_top == 'mobile'): ?>d-md-none<?php endif; ?> <?php if($lg_option_scroll_to_top == 'disable'): ?>d-none<?php endif; ?>">
		        	<i class="fas fa-chevron-up"></i>
		      	</div>
		    	<?php
		    	echo ob_get_clean(); 
		  	}
		}

		function lg_utility_bar(){
          $lg_site_config_utility_bar = get_option('lg_option_utility_bar');
          if($lg_site_config_utility_bar == 'all'){
            get_template_part("/templates/template-parts/header/utility-bar");
          }elseif($lg_site_config_utility_bar == 'mobile'){
            echo "<div class='d-none d-md-block'>";
            get_template_part("/templates/template-parts/header/utility-bar");
            echo "</div>";
          }
		}

		function lg_utility_left(){
			$lg_option_utility_bar_left = urldecode(get_option('lg_option_utility_bar_left'));
			echo do_shortcode($lg_option_utility_bar_left);
		}

		function lg_utility_right(){
			$lg_option_utility_bar_right = urldecode(get_option('lg_option_utility_bar_right'));
			echo do_shortcode($lg_option_utility_bar_right);
		}

		function lg_utility_center(){
			$lg_option_utility_bar_center = urldecode(get_option('lg_option_utility_bar_center'));
			echo do_shortcode($lg_option_utility_bar_center);
		}

		function lg_meta(){
			echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
		}

		function lg_flexible_content_layout_title( $title, $field, $layout, $i ) {
		  // remove layout title from text
		   $title = '';
		   $new_title = get_sub_field('block_title');
		   
		   if($new_title){
		     return $new_title;
		   }else{
		     return $title;
		   }
		}

	}

	new LGHooks();

?>