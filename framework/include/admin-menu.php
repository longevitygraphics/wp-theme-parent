<?php

if( function_exists('get_field') ) {
	$client_name = get_field('lg_site_config_client_name', 'options');
	$client_name = $client_name ? $client_name : 'LG Client';
}else{
	$client_name = 'LG Client';
}

// // Client Menu
// add_menu_page(
//     $client_name,
//     $client_name,
//     'edit_posts',
//     'lg_menu',
//     '',
//     'dashicons-admin-site',
//     2
// );

// add_submenu_page(
//     'lg_menu',
//     __('Client Instruction'), 
//     __('Client Instruction'), 
//     'edit_posts', 
//     'client-instruction', 
//     'lg_client_instruction'
// );


if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Site clone',
		'menu_title'	=> 'Site clone',
		'parent_slug'	=> 'Hidden'
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Client Info',
		'menu_title'	=> 'Client Info',
		'parent_slug'	=> 'hidden'
	));

	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Global Theme Options',
		'menu_title' 	=> 'Global Theme Options',
		'menu_slug' 	=> 'global-theme-options',
		'parent_slug'	=> 'lg_menu',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

}

$view = add_submenu_page(
    'hidden',
	__('Server Info', 'longevity-template'), 
	__('Server Info', 'longevity-template'), 
	'edit_posts', 
	'longevity_server_info', 
    array(&$this, 'load_view')
);
$this->views[$view] = 'server-info';

$view = add_submenu_page(
    'hidden',
	__('Plugins', 'longevity-template'), 
	__('Plugins', 'longevity-template'), 
	'edit_posts', 
	'longevity_plugins', 
    array(&$this, 'load_view')
);
$this->views[$view] = 'plugins';

$view = add_submenu_page(
    'hidden',
	__('Templates', 'longevity-template'), 
	__('Templates', "longevity-template"), 
	'edit_posts', 
	'longevity_templates', 
    array(&$this, 'load_view')
);
$this->views[$view] = 'templates';

$view = add_submenu_page(
    'hidden',
	__('Site Options', "longevity-template"), 
	__('Site Options', "longevity-template"), 
	'edit_posts', 
	'longevity_site_config', 
    array(&$this, 'load_view')
);
$this->views[$view] = 'site-config';

?>