<?php

  namespace Longevity\Framework\Helper;

  /**
   * Get Data
   *
   * Replace get_file_content()
   *
   * @param String  $String          - url
   *
   * @return String
   */
  // function lg_file_get_contents($url){

  //   $ch = curl_init();
  //   $timeout = 5;
  //   curl_setopt($ch,CURLOPT_URL,$url);
  //   curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  //   curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  //   $data = curl_exec($ch);
  //   curl_close($ch);
  //   return $data;

  // }

  if ( ! function_exists('lg_write_log')) {
	  function lg_write_log( $log ) {
		  if ( is_array( $log ) || is_object( $log ) ) {
			  error_log( print_r( $log, true ) );
		  } else {
			  error_log( $log );
		  }
	  }
  }

  function lg_startsWith($haystack, $needle){

      return strncmp($haystack, $needle, strlen($needle)) === 0;

  }

  function lg_endsWith($haystack, $needle){

      return $needle === '' || substr_compare($haystack, $needle, -strlen($needle)) === 0;
      
  }

  function lg_rm_dir($dir) {
   $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) { 
      (is_dir("$dir/$file")) ? lg_rm_dir("$dir/$file") : unlink("$dir/$file"); 
    } 
    return rmdir($dir); 
  }

?>