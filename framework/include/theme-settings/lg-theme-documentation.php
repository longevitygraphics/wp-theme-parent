<style>
	.wrap{
		padding: 10px 15px;
		background-color: #fff;
		border: 1px solid #E5E5E5;
	}

	h1{
		font-size: 20px !important;
	}

	h2{
		font-size: 17px !important;
	}

	h3{
		font-size: 14px !important;
	}

	html, body{
		font-size: 13px !important;
	}

</style>

<div class="wrap">
	<section>
		<h1>LG Theme Options</h1>
		<div class="wrap">
			
			<div class="wrap">
				<p>THEME OPTION - <a href="/wp-admin/admin.php?page=acf-options-social-media">Social Media</a> - Active your social media icons and display on the page using shortcode.</p>

				<ul>
					<li>Required Dependency: Fontawesome</li>
				</ul>

				<p>
					<b>Shortcodes:</b><br>
					list all ‘enabled’ social media icons	–	[lg-social-media]<br>
					list specified ‘enabled’ social media icons	–	[lg-social-media list="google+, youtube, instagram"]<br>
				</p>

				<p>
					Existing Social Medias(You can copy name from below for frontend display)<br>
					facebook, twitter, instagram, pinterest, youtube, linkedin, google+, rss<br>
				</p>
			</div>

			<div class="wrap">
				<p>THEME OPTION - <a href="/wp-admin/admin.php?page=acf-options-contact">Contact Info</a> - Using shortcode to develop the theme with contact info settings.</p>

				<ul>
					<li>Required Dependency: None</li>
					<li>Admin Menu: “LG Theme Settings -> Contact”</li>
				</ul>

				<p>
					<b>Shortcodes:</b><br>
					[lg-address1] [lg-address2] [lg-city] [lg-province] [lg-country] [lg-postcode]<br>
					[lg-contact-person] [lg-job-title] [lg-phone-main] [lg-phone-alt] [lg-fax] [lg-email] [lg-website]
				</p>
			</div>

			<div class="wrap">
				<p>THEME OPTION - <a href="/wp-admin/admin.php?page=acf-options-analytics-tracking">Analytics Tracking</a> - Set up tracking tags on website</p>

				<ul>
					<li>Required Dependency: None</li>
					<li>Admin Menu: “LG Theme Settings -> Analytics Tracking</li>
				</ul>

				<p>The only required setup is the tracking code. GTM installation recommended.</p>
			</div>
		</div>
	</section>
</div>