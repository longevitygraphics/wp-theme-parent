<div id="fontawesome-modal-wrapper">
	<div class="fa-toolbar">
		<input type="text" id="fa-search">
		<a class="fa-close">close</a>
	</div>
	<div id="fontawesome-modal">
		
	</div>
</div>

<script>
	(function($) {
	    $(document).ready(function(){

	    	load_fontawesome();

	    	$('body').on('click', '.fontawesome-set-icon', function(){
	    		$('#fontawesome-modal-wrapper').show().attr('active-field', $(this).parent().prev().find('.icon').attr('name'));
	    	});

	    	$('body').on('click', '#fontawesome-modal .fontawesome-icon i', function(){
	    		var parent = $(this).closest('#fontawesome-modal-wrapper');
	    		var active_field = parent.attr('active-field');
	    		var current_classes = $(this).attr('class');

	    		$('.icon[name='+ active_field +']').empty().append('<i class="'+ current_classes +'"></i>').parent().prev().find('.fontawesome-icon-input').val(current_classes);
	    		parent.attr('active-field', '').hide();
	    	});



			function load_fontawesome(){
				var data = {
					'action': 'lg_get_fontawesome'
				};

				jQuery.post(ajaxurl, data, function(response) {
					var data = JSON.parse(response);
					var result = Object.keys(data).map(function(key) {
						$('#fontawesome-modal').append('<div class="fontawesome-icon"><i class="'+key+'"></i></div>');
					  	return {"key":key, "data":data[key]};
					});

					$('#fontawesome-modal-wrapper #fa-search').on('keyup', function(key){
						var data = $(this).val();
						$('#fontawesome-modal .fontawesome-icon i').each(function(){
							var classes = $(this).attr('class')
							var classes_array = classes.split(' ');
							var check = false;

							classes_array.forEach(function(current){
								if(current.indexOf(data) != -1){
									check = true;
								}
							});

							if(check == true){
								$(this).closest('.fontawesome-icon').removeClass('d-none');
							}else{
								$(this).closest('.fontawesome-icon').addClass('d-none');
							}
						});
	    			});
				});
		    }
		});
	}(jQuery));
</script>