<?php

// Framework Menu
$args = array(
	'id'    => 'lg_framework',
	'title' => 'Longevity Framework',
);
$wp_admin_bar->add_node( $args );

// add Site Info Page
$args = array(
	'id'     => 'site-info',
	'title'  => 'Server Info',
	'parent' => 'lg_framework',
	'href'	 => get_site_url() . '/wp-admin/admin.php?page=longevity_server_info'
);
$wp_admin_bar->add_node( $args );

// $args = array(
// 	'id'     => 'development_setup',
// 	'parent' => 'lg_framework',
// 	'meta'   => array( 'class' => 'development_setup' ),
// );
// $wp_admin_bar->add_group( $args );

// // add an item to our group item
// $args = array(
// 	'id'     => 'development_setup_main',
// 	'title'  => 'Development Setup',
// 	'parent' => 'development_setup',
// );
// $wp_admin_bar->add_node( $args );

// // add Plugins Page
// $args = array(
// 	'id'     => 'plugins',
// 	'title'  => 'Plugins Management',
// 	'parent' => 'development_setup_main',
// 	'href'	 => get_site_url() . '/wp-admin/admin.php?page=longevity_plugins'
// );
// $wp_admin_bar->add_node( $args );

// // add another child item to first group
// $args = array(
// 	'id'     => 'theme_clone',
// 	'title'  => 'Theme Clone',
// 	'parent' => 'development_setup_main',
// 	'href'	 => get_site_url() . '/wp-admin/admin.php?page=longevity_templates'
// );
// $wp_admin_bar->add_node( $args );

	$args = array(
	'id'     => 'theme_setup',
	'parent' => 'lg_framework',
	'meta'   => array( 'class' => 'theme_setup' ),
);
$wp_admin_bar->add_group( $args );

// add an item to our group item
$args = array(
	'id'     => 'theme_setup_main',
	'title'  => 'Theme Setup',
	'parent' => 'theme_setup'
);
$wp_admin_bar->add_node( $args );

// $args = array(
// 	'id'     => 'lg_client_info',
// 	'title'  => 'Client Info',
// 	'parent' => 'theme_setup_main',
// 	'href'	 => get_site_url() . '/wp-admin/admin.php?page=acf-options-client-info'
// );
// $wp_admin_bar->add_node( $args );

$args = array(
	'id'     => 'lg_site_config',
	'title'  => 'Site Options',
	'parent' => 'theme_setup_main',
	'href'	 => get_site_url() . '/wp-admin/admin.php?page=longevity_site_config'
);
$wp_admin_bar->add_node( $args );

/*$args = array(
	'id'     => 'social_media',
	'title'  => 'Social Media',
	'parent' => 'theme_setup_main',
	'href'	 => get_site_url() . '/wp-admin/admin.php?page=acf-options-social-media'
);
$wp_admin_bar->add_node( $args );

$args = array(
	'id'     => 'tracking',
	'title'  => 'Analytics Tracking',
	'parent' => 'theme_setup_main',
	'href'	 => get_site_url() . '/wp-admin/admin.php?page=acf-options-analytics-tracking'
);
$wp_admin_bar->add_node( $args );*/

// Remove Item From Admin Bar
//$wp_admin_bar->remove_node( 'gform-forms' );
update_option( 'gform_enable_toolbar_menu', False );

?>