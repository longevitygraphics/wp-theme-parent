<?php

use Longevity\Framework\Plugins;
use Longevity\Framework\Helper;
use Longevity\Framework\Fontawesome;


	class LGAjax{

		function __construct(){
			add_action( 'wp_ajax_lg_plugin_install', array( $this, 'lg_plugin_install' ) );
			add_action( 'wp_ajax_lg_plugin_uninstall', array( $this, 'lg_plugin_uninstall' ) );
			add_action( 'wp_ajax_lg_plugin_activate', array( $this, 'lg_plugin_activate' ) );
			add_action( 'wp_ajax_lg_plugin_deactivate', array( $this, 'lg_plugin_deactivate' ) );

			add_action( 'wp_ajax_lg_template_install', array( $this, 'lg_template_install' ) );

			add_action( 'wp_ajax_lg_update_config', array( $this, 'lg_update_config' ) );
			add_action( 'wp_ajax_lg_get_config', array( $this, 'lg_get_config' ) );

			add_action( 'wp_ajax_lg_get_fontawesome', array( $this, 'lg_get_fontawesome' ) );
		}

		function lg_plugin_install(){
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));
			Plugins\plugin_install($data->zipUrl, $data->folderName);
		}

		function lg_plugin_uninstall(){
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));
			Helper\lg_write_log($data->folderName);
			Plugins\plugin_uninstall($data->folderName);
		}

		function lg_plugin_activate(){
			$data = $_REQUEST['data'];
			activate_plugin($data);
		}

		function lg_plugin_deactivate(){
			$data = $_REQUEST['data'];
			deactivate_plugins($data);
		}

		function lg_template_install(){
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));

			$migrate = new LGMigrate();
			$migrate->startMigrate($data->zipUrl);

			switch_theme( 'wp-theme-child' );
		}

		function lg_update_config(){
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));

			if($data && is_array($data)){
				foreach ($data as $key => $field) {
					if($field->type == 'textarea'){
						$value = urldecode($field->value);
					}else{
						$value = $field->value;
					}

					$success = add_option($field->name, $value);

					if(!$success){
						update_option($field->name, $value);
					}
				}
			}else{
				return false;
			}
		}

		function lg_get_config(){
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));

			if($data && is_array($data)){
				foreach ($data as $key => $field) {
					$field->value = get_option($field->name);
				}
			}else{
				echo false;
				wp_die();
			}

			echo json_encode($data);
			wp_die();
		}

		function lg_get_fontawesome(){
			echo json_encode(Fontawesome\get_icons());
			wp_die();
		}

	}

	new LGAjax();

?>