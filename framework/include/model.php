<?php

	namespace Longevity\Framework\Models;

	class LongevityPlugin{

		public $zipUrl;
		public $directoryName;
		public $pluginFilePath;

		function __construct($zipUrl, $directoryName, $pluginFilePath){

			$this->zipUrl = $zipUrl;
			$this->directoryName = $directoryName;
			$this->pluginFilePath = $pluginFilePath;

		}

	}

?>