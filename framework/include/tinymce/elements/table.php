<?php

	/*
	*	Build bootstrap table classes into Tinymce
	*/

	global $lg_tinymce_table;
    
    $selector = 'table';
    $lg_tinymce_table = array(
        'title' => 'Table',
        'items' =>  array(
			array(
				'title' => 'Table',
				'selector' => $selector,
				'classes'	=> 'table'
			),
			array(
				'title' => 'Table Dark',
				'selector' => $selector,
				'classes'	=> 'table-dark'
			),
			array(
				'title' => 'Table Striped',
				'selector' => $selector,
				'classes'	=> 'table-striped'
			),
			array(
				'title' => 'Table Hover',
				'selector' => $selector,
				'classes'	=> 'table-hover'
			),
			array(
				'title' => 'Table Border',
				'selector' => $selector,
				'classes'	=> 'table-bordered'
			),
			array(
				'title' => 'Table No Border',
				'selector' => $selector,
				'classes'	=> 'table-borderless'
			),
			array(
				'title' => 'Table Responsive XS',
				'selector' => $selector,
				'classes'	=> 'table-responsive'
			),
			array(
				'title' => 'Table Responsive SM',
				'selector' => $selector,
				'classes'	=> 'table-responsive-sm'
			),
			array(
				'title' => 'Table Responsive MD',
				'selector' => $selector,
				'classes'	=> 'table-responsive-md'
			),
			array(
				'title' => 'Table Responsive LG',
				'selector' => $selector,
				'classes'	=> 'table-responsive-lg'
			),
			array(
				'title' => 'Table Responsive XL',
				'selector' => $selector,
				'classes'	=> 'table-responsive-xl'
			)
        )
    );

?>