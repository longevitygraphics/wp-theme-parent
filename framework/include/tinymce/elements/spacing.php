<?php

	/*
	*	Build Bootstrap spacing classes into Tinymce such as padding and margin, Not including responsive classes yet, to be add in the future development.
	*/

	global $lg_tinymce_spacing;

	$selector = 'div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote';
	$padding = array();
	$margin = array();

	$direction = array(
		'Left'=> 'l',
		'Right'=> 'r',
		'Top'=> 't',
		'Bottom'=> 'b'
	);

	$spacing = array(
		'No Spacing'=> '0',
		'Extra Small'=> '1',
		'Small'=> '2',
		'Medium'=> '3',
		'Large'=> '4',
		'Extra Large'=> '5'
	);

	// Padding
    $padding_direction_array = array();

    foreach ($direction as $direction_key => $direction_value) {
		$spacing_array = array();
		foreach ($spacing as $spacing_key => $spacing_value) {
			array_push($spacing_array, array(
				'title' => $spacing_key,
	            'selector' => $selector,
	            'classes' => 'p' . $direction_value . '-' . $spacing_value
			));
		}

		array_push($padding_direction_array, array(
		    'title' => $direction_key,
		    'items' =>  $spacing_array
		));
	}


	// Margin
    $margin_direction_array = array();

    foreach ($direction as $direction_key => $direction_value) {
		$spacing_array = array();
		foreach ($spacing as $spacing_key => $spacing_value) {
			array_push($spacing_array, array(
				'title' => $spacing_key,
	            'selector' => $selector,
	            'classes' => 'm' . $direction_value . '-' . $spacing_value
			));
		}

		array_push($margin_direction_array, array(
		    'title' => $direction_key,
		    'items' =>  $spacing_array
		));
	}

    $lg_tinymce_spacing = array(
    	'title'=> 'Spacing',
    	'items'=> array(
	        array(
	        	'title'=> 'Padding',
		        'items'=> $padding_direction_array
	        ),
	        array(
	        	'title'=> 'margin',
		        'items'=> $margin_direction_array
	        )
    	)
    );

?>