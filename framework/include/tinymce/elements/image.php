<?php

	/*
	*	Build bootstrap color classes into Tinymce
	*/

	global $lg_tinymce_image;
	
	$selector = 'img';
	$lg_tinymce_image = array(
	    'title' => 'Image',
	    'items' =>  array(
	    	array(
				'title' => 'Full',
	            'selector' => $selector,
	            'classes' => 'img-full'
			),
			array(
				'title' => 'Thumbnail',
	            'selector' => $selector,
	            'classes' => 'img-thumbnail'
			),
			array(
				'title' => 'Rounded',
	            'selector' => $selector,
	            'classes' => 'rounded'
			),
	    )
	)

?>