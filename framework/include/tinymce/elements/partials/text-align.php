<?php
	
	$text_align_array = array();
	$breakpoints = array('','sm', 'md', 'lg', 'xl');
	$alignments = array('left', 'center', 'right');

	foreach ($breakpoints as $breakpoint_key => $breakpoint) {
		$breakpoint_array = array();

		foreach ($alignments as $alignment_key => $alignment) {

			if($breakpoint == ''){
				array_push($breakpoint_array, array(
					'title' => ucwords($alignment),
		            'selector' => 'h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,p,div,blockquote',
		            'classes' => 'text-'. $alignment
				));
			}else{
				array_push($breakpoint_array, array(
					'title' => ucwords($alignment),
		            'selector' => 'h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,p,div,blockquote',
		            'classes' => 'text-'. $breakpoint . '-' . $alignment
				));
			}

		}

		if($breakpoint == ''){
			array_push($text_align_array, array(
				'title' => 'default',
	            'items' => $breakpoint_array
			));
		}else{
			array_push($text_align_array, array(
				'title' => ucwords($breakpoint),
	            'items' => $breakpoint_array
			));			
		}

	}

?>