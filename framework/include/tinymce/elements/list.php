<?php

	/*
	*	Build bootstrap li classes into Tinymce
	*/

	global $lg_tinymce_list;

	$selector = 'ul, ol';
	$lg_tinymce_list = array(
	    'title' => 'List',
	    'items' =>  array(
			array(
				'title' => 'Unstyled',
				'selector' => $selector,
				'classes'	=> 'list-unstyled'
			)
	    )
	)

?>