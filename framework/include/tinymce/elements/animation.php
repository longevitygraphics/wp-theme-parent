<?php

	/*
	*	Build classes from animate.css into tinymce
	*/

	global $lg_tinymce_animation;

	$selector = 'span';
	$initArray = array(
		array(
			'Animation Control'=> array(
				'fast',
				'faster',
				'slow',
				'slower',
				'delay-2s',
				'delay-3s',
				'delay-4s',
				'delay-5s'
			),
			'Custom'=> array(
				'flash',
				'pulse',
				'rubberBand',
				'shake',
				'headShake',
				'swing',
				'tada',
				'wobble',
				'jello',
				'heartBeat',
				'hinge',
				'jackInTheBox'
			),
			'Bounce'=> array(
				'bounce',
				'bounceIn',
				'bounceInDown',
				'bounceInLeft',
				'bounceInRight',
				'bounceInUp',
				'bounceOut',
				'bounceOutDown',
				'bounceOutLeft',
				'bounceOutRight',
				'bounceOutUp'
			),
			'Fade'=> array(
				'fadeIn',
				'fadeInDown',
				'fadeInDownBig',
				'fadeInLeft',
				'fadeInLeftBig',
				'fadeInRight',
				'fadeInRightBig',
				'fadeInUp',
				'fadeInUpBig',
				'fadeOut',
				'fadeOutDown',
				'fadeOutDownBig',
				'fadeOutLeft',
				'fadeOutLeftBig',
				'fadeOutRight',
				'fadeOutRightBig',
				'fadeOutUp',
				'fadeOutUpBig'
			),
			'Flip'=> array(
				'flipInX',
				'flipInY',
				'flipOutX',
				'flipOutY'
			),
			'Light Speed'=> array(
				'lightSpeedIn',
				'lightSpeedOut'
			),
			'Rotate'=> array(
				'rotateIn',
				'rotateInDownLeft',
				'rotateInDownRight',
				'rotateInUpLeft',
				'rotateInUpRight',
				'rotateOut',
				'rotateOutDownLeft',
				'rotateOutDownRight',
				'rotateOutUpLeft',
				'rotateOutUpRight'
			),
			'Roll'=> array(
				'rollIn',
				'rollOut'
			),
			'Zoom'=> array(
				'zoomIn',
				'zoomInDown',
				'zoomInLeft',
				'zoomInRight',
				'zoomInUp',
				'zoomOut',
				'zoomOutDown',
				'zoomOutLeft',
				'zoomOutRight',
				'zoomOutUp'
			),
			'Slide'=> array(
				'slideInDown',
				'slideInLeft',
				'slideInRight',
				'slideInUp',
				'slideOutDown',
				'slideOutLeft',
				'slideOutRight',
				'slideOutUp'
			)
		)
	);

	$returnArray = [];

	if($initArray && is_array($initArray)){
		foreach ($initArray as $lvl1key => $lvl1value) {
			if(is_array($lvl1value)){
				foreach ($lvl1value as $lvl2key => $lvl2value) {
					$lvl2Array = array();
					if(is_array($lvl2value)){
						foreach ($lvl2value as $lvl3key => $lvl3value) {
							if($lvl2key == 'Animation Control'){
								array_push($lvl2Array, array(
									'title' => ucwords($lvl3value),
						            'inline' => $selector,
			            			'classes' => $lvl3value,
								));
							}else{
								array_push($lvl2Array, array(
									'title' => ucwords($lvl3value),
						            'inline' => $selector,
			            			'classes' => 'animated viewport-animation',
			            			'attributes' => array(
								        'animation' => $lvl3value
								    )
								));
							}
						}
						$lvl1Array = array(
						    'title' => $lvl2key,
						    'items' =>  $lvl2Array
						);
					}
					array_push($returnArray, $lvl1Array);
				}		
			}else{
				array_push($returnArray, array(
					'title' => ucwords($lvl1value),
		            'inline' => $selector,
		            'classes' => 'animated viewport-animation',
		            'attributes' => array(
				        'animation' => $lvl1value
				    )
				));			
			}
		}
	}

	$lg_tinymce_animation = array(
	    'title' => 'Animation',
	    'items' =>  $returnArray
	);

?>