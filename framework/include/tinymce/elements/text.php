<?php

	/*
	*	Build bootstrap text classes into Tinymce
	*/

	global $lg_tinymce_text;

	$selector = 'span';
	$title_size = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'small'];
	$title_size_array = [];

	if($title_size && is_array($title_size)){
		foreach ($title_size as $key => $value) {
			array_push($title_size_array, array(
				'title' => ucwords($value),
	            'inline' => $selector,
	            'classes' => $value
			));
		}
	}
    
    /* Font Weight */

    $font_weight = array(
    	array(
			'title' => 'Normal',
            'inline' => $selector,
            'classes' => 'font-weight-normal'
		),
    	array(
			'title' => 'Light',
            'inline' => $selector,
            'classes' => 'font-weight-light'
		),
		array(
			'title' => 'Bold',
            'inline' => $selector,
            'classes' => 'font-weight-bold'
		)
    );

    /* end Font Weight */

    /* Font Case */

    $font_case = array(
    	array(
			'title' => 'Lower Case',
            'inline' => $selector,
            'classes' => 'text-lowercase'
		),
		array(
			'title' => 'Uppercase',
            'inline' => $selector,
            'classes' => 'text-uppercase'
		),
		array(
			'title' => 'Capitalize',
            'inline' => $selector,
            'classes' => 'text-capitalize'
		)
    );

    /* end Font Case */

    /* Font Color */

    $colors = [];
	$color_array = [];

	if(function_exists('get_field_object')){
		$acf_colors = get_field_object('field_5bec589d4e39d', 'option');
		if (!is_array($acf_colors)) {
		    $acf_colors = array();
		}
		$acf_colors = $acf_colors['choices'] ?? array();
		$colors = array_merge($colors, $acf_colors);
	}

	if($colors && is_array($colors)){
		foreach ($colors as $key => $value) {
			array_push($color_array, array(
				'title' => ucwords($value),
	            'inline' => 'span',
	            'classes' => 'text-'.$value
			));
		}
	}

	/* End Font Color */

	/* Text alignment */

	include 'partials/text-align.php';

    /* End Text alignment */

    $lg_tinymce_text = array(
    	'title'=> 'Text',
    	'items'=> array(
    		array(
	        	'title'=> 'Color',
	        	'items'=> $color_array
	        ),
	        array(
	        	'title'=> 'Size',
		        'items'=> $title_size_array
	        ),
	        array(
	        	'title'=> 'Weight',
		        'items'=> $font_weight
	        ),
	        array(
	        	'title'=> 'Case',
		        'items'=> $font_case
	        ),
	        array(
	        	'title'=> 'Alignment',
	        	'items'=> $text_align_array
	        )
    	)
    );

?>