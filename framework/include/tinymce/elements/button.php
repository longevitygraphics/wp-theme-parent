<?php

	/*
	*	Bootstrap button classes
	*/

	global $lg_tinymce_button;

	$colors = [];
	$button_array = [];

	// Getting all the colors from ACF - Global Color Field Group
	if(function_exists('get_field_object')){
		$acf_colors = get_field_object('field_5bec589d4e39d', 'option');
		if (!is_array($acf_colors)) {
		    $acf_colors = array();
		}
		$acf_colors = $acf_colors['choices'] ?? array();
		$colors = array_merge($colors, $acf_colors);
	}

	// Build Bootstrap button options for each color, both btn- and btn-outline- classes.
	if($colors && is_array($colors)){
		foreach ($colors as $key => $value) {
			array_push($button_array, array(
				'title' => ucwords($value),
	            'selector' => 'a',
	            'classes' => 'btn btn-'.$value
			));

			array_push($button_array, array(
				'title' => ucwords($value) . ' Outline',
	            'selector' => 'a',
	            'classes' => 'btn btn-outline-'.$value
			));
		}
	}

	// Build Bootstrap button size options
	$button_size = array(
    	array(
			'title' => 'Small',
            'selector' => 'a',
            'classes' => 'btn-sm'
		),
		array(
			'title' => 'Large',
            'selector' => 'a',
            'classes' => 'btn-lg'
		)
    );

    /******************************************/

	$lg_tinymce_button = array(
    	'title'=> 'Buttons',
    	'items'=> array(
	        array(
	        	'title'=> 'Color & Type',
		        'items'=> $button_array
	        ),
	        array(
	        	'title'=> 'Size',
		        'items'=> $button_size
	        )
    	)
    );

?>