<?php

	include 'elements/table.php';
	include 'elements/spacing.php';
	include 'elements/text.php';
	include 'elements/list.php';
	include 'elements/image.php';
	include 'elements/animation.php';
	include 'elements/button.php';

	// Register format button
	function lg_mce_buttons_2( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Insert items to format button
	function lg_mce_before_init_insert_formats( $init_array ) { 

		global $lg_tinymce_table;
		global $lg_tinymce_spacing;
		global $lg_tinymce_text;
		global $lg_tinymce_list;
		global $lg_tinymce_image;
		global $lg_tinymce_button;
		global $lg_tinymce_custom;
		global $lg_tinymce_animation;

	    $style_formats = array(
	    	$lg_tinymce_text,
			$lg_tinymce_button,
			$lg_tinymce_image,
			$lg_tinymce_list,
	        $lg_tinymce_table,
	        $lg_tinymce_spacing,
	        $lg_tinymce_animation  
	    );

	    if($lg_tinymce_custom){
	    	array_push($style_formats, $lg_tinymce_custom);
	    }

	    $init_array['style_formats'] = json_encode( $style_formats );

	    return $init_array; 
	  
	} 

	// Remove some buttons that don't need
	function lg_remove_tiny_mce_buttons( $buttons ) {
	    $remove_buttons = array(
	        'wp_more', // read more link
	        'dfw', // distraction free writing mode
	        'wp_adv', // kitchen sink toggle (if removed, kitchen sink will always display)
	    );
	    foreach ( $buttons as $button_key => $button_value ) {
	        if ( in_array( $button_value, $remove_buttons ) ) {
	            unset( $buttons[ $button_key ] );
	        }
	    }
	    return $buttons;
	}

	function lg_override_tinymce_options($initArray) {
	    $opts = '*[*]';
	    $initArray['valid_elements'] = $opts;
	    $initArray['extended_valid_elements'] = $opts;
	    return $initArray;
	}


	add_filter( 'tiny_mce_before_init', 'lg_mce_before_init_insert_formats' );  
	add_filter( 'mce_buttons', 'lg_remove_tiny_mce_buttons');
	add_filter( 'mce_buttons', 'lg_mce_buttons_2' );
 	add_filter('tiny_mce_before_init', 'lg_override_tinymce_options');

?>