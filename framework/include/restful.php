<?php
class LGRestApi {

	function __construct() {
		add_action(
			'rest_api_init',
			function () {
				register_rest_route(
					'longevity_framework/v1',
					'/plugins',
					array(
						'methods'             => 'GET',
						'callback'            => array( $this, 'get_plugins' ),
						'permission_callback' => '__return_true',
					)
				);
			}
		);
	}

	function get_plugins() {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		$plugins       = get_plugins();
		$plugins_array = array();

		if ( $plugins && is_array( $plugins ) ) {
			foreach ( $plugins as $key => $plugin ) {
				$plugin['PluginFile'] = $key;

				if ( is_plugin_active( $key ) ) {
					$plugin['Active'] = true;
				} else {
					$plugin['Active'] = false;
				}
				array_push( $plugins_array, $plugin );
			}
		}
		wp_send_json( $plugins_array );
	}
}


	new LGRestApi();

