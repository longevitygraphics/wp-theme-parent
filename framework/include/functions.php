<?php

	function lg_theme_documentation(){
		require_once 'theme-settings/lg-theme-documentation.php';
	}

	function lg_client_instruction(){
		require_once 'theme-settings/lg-client-instruction.php';
	}

	function lg_dependency_check(){
		if (!function_exists('get_plugins')) {
	        require_once ABSPATH . 'wp-admin/includes/plugin.php';
	    }
	    $plugins = get_plugins();
	    $result = true;

	    if (isset($plugins['advanced-custom-fields-pro/acf.php'])) {
	    	
			if(!is_plugin_active('advanced-custom-fields-pro/acf.php')){
				$result = false;
			}else if($plugins['advanced-custom-fields-pro/acf.php']['Version'] < 5.6){
				$result = false;
			}
	    }else{
	    	$result = false;
	    }

		return $result;
	}
	
?>