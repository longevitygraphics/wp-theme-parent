<?php
	/* Include theme functions */
	$function_dir_path = get_template_directory();
	$function_file_path = [
		// '/libs/theme-supports.php',
		// '/libs/helper.php',
		// '/libs/widgets.php',
		// '/libs/class-wp-bootstrap-navwalker.php',
		// '/libs/bootstrap-pagination.php',
		'/framework/framework.php',
		//'/layouts/layouts.php'
	];

	if($function_file_path && is_array($function_file_path)){
		foreach ($function_file_path as $key => $value) {
			if (file_exists($function_dir_path . $value)) {
			    require_once($function_dir_path . $value);
			}
		}
	}

	/* end */
?>
